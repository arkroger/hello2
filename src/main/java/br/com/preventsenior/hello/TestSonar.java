package br.com.preventsenior.hello;

import org.springframework.stereotype.Component;

@Component
public class TestSonar {

  public void teste() throws Exception {

    this.testeint(6);

    this.testeint(4);
    System.out.println("teste 2");
  }

  private void testeint(int i) {
    System.out.println(i);
  }
}
