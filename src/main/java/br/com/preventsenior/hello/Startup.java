package br.com.preventsenior.hello;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Startup {

  private static final Logger LOGGER = LoggerFactory.getLogger(Startup.class);

  @EventListener(ApplicationReadyEvent.class)
  public void doSomethingAfterStartup() {

    LOGGER.info("########################## TODAS VARIAVEIS #################");
    Map<String, String> env = System.getenv();
    env.forEach((k, v) -> LOGGER.info(k + ":" + v));

    LOGGER.info("########################## PRINCIPAIS VARIAVEIS #################");
    LOGGER.info("SPRING_PROFILES_ACTIVE: {}", System.getenv("SPRING_PROFILES_ACTIVE"));
    LOGGER.info("NEW_RELIC_APP_NAME: {}", System.getenv("NEW_RELIC_APP_NAME"));
    LOGGER.info("JASYPT_ENCRYPTOR_PASSWORD: {}", System.getenv("JASYPT_ENCRYPTOR_PASSWORD"));
  }
}
