#Badges

[![DeepSource](https://deepsource.io/bb/arkroger/hello2.svg/?label=active+issues&show_trend=true&token=Trv0CfxaqKrfLwD5rXFjwnK-)](https://deepsource.io/bb/arkroger/hello2/?ref=repository-badge)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=bugs)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=coverage)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=arkroger_hello2&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=arkroger_hello2)](https://sonarcloud.io/summary/new_code?id=arkroger_hello2)

# Read Me First
The following was discovered as part of building this project:

* The original package name ' br.com.preventsenior.hello' is invalid and this project uses 'br.com.preventsenior.hello' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.6/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.6.6/maven-plugin/reference/html/#build-image)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.6.6/reference/htmlsingle/#using-boot-devtools)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.6.6/reference/htmlsingle/#production-ready)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)

